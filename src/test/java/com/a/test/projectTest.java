package com.a.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.a.CRMDriver;
import com.a.models.Automobile;
import com.a.models.Automobiles;
import com.a.models.Bid;
import com.a.models.Customer;
import com.a.models.Employee;


public class projectTest {
	
	final static Logger logger = Logger.getLogger(CRMDriver.class);
	

		
	@BeforeClass
	public static void beforeAll() {
		
		System.out.println("========Before All=======");
	}
	
	@AfterClass
	public static void afterAll() {
		System.out.println("========after All=======");
	}
	
	@Before
	public void beforeTest() {
		System.out.println("========Before test=======");
	}
	
	@After
	public void afterTest() {
		System.out.println("========after test=======");
	}
	
	
	@Test
	public void testDriver() {
	CRMDriver crmdriver = new CRMDriver();
	assertNotNull(crmdriver);
	}
	
	
	@Test
	public void testAutoMap() {
	Automobile automobile = new Automobile("testMobile", 5000.00);
	assertNotNull(automobile);
	}
	
	@Test
	public void testCustomerMap() {
	Customer customer = new Customer("testGuy","password");
	assertNotNull(customer);
	}
	
	@Test
	public void testEmployeeMap() {
	Employee employee = new Employee("testGuy","password");
	assertNotNull(employee);
	}
	
	
	@Test
	public void testBid() {
	Bid bid = new Bid(555555, 11111111, "testcarmodelt", 70.00);
	assertNotNull(bid);
	}
	
	

}