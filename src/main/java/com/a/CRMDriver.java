package com.a;


import java.util.List;
import java.util.Scanner;

import com.a.models.Automobile;
import com.a.models.Bid;
import com.a.services.AutomobileService;
import com.a.services.BidService;
import com.a.services.CustomerService;
import com.a.services.EmployeeService;






public class CRMDriver {
	
	
	public static void main(String[] args) {
		
		Scanner sMain = new Scanner(System.in);
		String input;
		CustomerService cs = new CustomerService();
		EmployeeService es = new EmployeeService();
		
		do {
			System.out.println("\nplease type \" customer login\", \"new customer\" , \"employee login\" or \"quit\" below \n");

			input = sMain.nextLine();
			switch (input) {
			
			case "customer login":
				
				customerLoop();
	
				break;

			case "new customer":
				cs.createNewCustomer();
				break;

			case "employee login":
				employeeLoop();
//				
				break;
				
//			case "new employee":
//				EmployeeService.createNewEmployee();
//				break;
			
			case "quit":
				System.out.println("Exiting program");
				break;

			default:
				System.out.println("\nInput not recognized\n");

			}
			
		} while (input.equals("quit") != true);
		
		

	}
	
	//contains logic for customer loop
	public static void customerLoop() {
		Scanner scl = new Scanner(System.in);
		CustomerService cs = new CustomerService();
		BidService bs = new BidService();
		AutomobileService as = new AutomobileService();
		
		String[] pair = cs.customerVerifier();
		String cID = pair[1];

		if(pair[0].equals("a")) {
		String input2 ;
		do {
			System.out.println("\nWelcome" + "\nplease type \"view cars\" , \"place bid\" , \"my cars\" ,\"show payments\" \"quit\" \n");
			input2 = scl.nextLine();
			switch(input2) {
			case "view cars":
				List<Automobile> carsForSale = cs.carsForSale();
				System.out.println(carsForSale);
				break;
			case "place bid":
				bs.placeBid(cID);
				break;
				
			case "my cars":
				List<Automobile> carsOwnedByCustomer = cs.carsThatACustomerOwns(cID);
				System.out.println(carsOwnedByCustomer);
				break;
				
			case "show payments":
				as.calculateRemaingPayments(cID);
				break;	
				
			case "quit":
				break;
			}
			
		}while(input2.equals("quit")!=true);
	
	}
	
	}
	
	//contains logic for employee loop
	public static void employeeLoop() {
		Scanner sel = new Scanner(System.in);
		String input4;
		EmployeeService es = new EmployeeService();
		BidService bs = new BidService();
		Boolean login = es.employeeVerifier();
		
		if(login==true) {
			
		do {
			
			System.out.println("\ntype \"add car\" , \"remove car\" ,\"3\" to show all bids , \"reject bid\" , "
					+"\"accept bid\", \"outstanding payments\" or \"quit\"\n");
			input4 = sel.nextLine();
			switch(input4) {
				case "add car":
					es.createNewAutomobile();
				break;
				
				case "remove car":
					es.removeFromLot();
					break;
				
				case "3":
					List<Bid> bidList =bs.showAllBids();
					System.out.println(bidList);
					break;
					
				case "reject bid":
					bs.rejectBid();
					break;
					
				case "accept bid":
					bs.acceptBid();
					break;
					
				case "outstanding payments":
					es.showAllPayments();
					break;
					
				case"quit":
					System.out.println("\nExiting employee menu\n");
					break;
				
				
				default:
					System.out.println("\nInput not recognized\n");
				
				
			}
		}while(input4.equals("quit")!=true);
	}
	
	}
	
	
	//created this constructor for junit to have something to work with
	public CRMDriver() {
		super();
		// TODO Auto-generated constructor stub
	};
	
	

}
