package com.a.services;

import java.util.List;
import java.util.Scanner;
import com.a.dao.CustomerDao;
import com.a.models.Automobile;

public class CustomerService {
	
	
	private CustomerDao cd;
	
	public CustomerService() {
		cd = new CustomerDao();
	}
	
	
	public List<Automobile> carsForSale() {
		 return cd.findAll();
	}
	
	
	public List<Automobile> carsThatACustomerOwns(String s) {
		return cd.findByUsername(s); 
	}
	
	public void showMonthlyPayment() {

	}


	public String[] customerVerifier() {
		String[] ans = new String[2];
	
		Scanner s1 = new Scanner(System.in);
		
		System.out.println("\nEnter your username below:\n");
		String userId = s1.nextLine();
		
		System.out.println("\nEnter your password below:\n");
		String passwordAttempt = s1.nextLine();

		
		String pass = cd.findById(userId);
		
		
		
		
		
		if(passwordAttempt.equals(pass)){
			ans[0]="a";
			ans[1]=userId;
			return ans;
		}else {
			System.out.println("incorrect customer password");
		}
		
		ans[0]="b";
		ans[1]=userId;
		return ans;
	}
	
	
	


	public void createNewCustomer() {
		Scanner scnc = new Scanner(System.in);
		System.out.println("\nWelcome new customer\nPlease create a new account\nEnter your username below\n");
		String username = scnc.nextLine();
				
		System.out.println("\nEnter your new password below\n");
		String password = scnc.nextLine();
		
		cd.insert(username,password);
	
		System.out.println("\nYour new account has been created "+ username+"\n");
		
	}




}
