package com.a.services;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.a.dao.AutomobileDao;
import com.a.dao.EmployeeDao;
import com.a.models.Automobile;

public class EmployeeService {

	private EmployeeDao ed;
	private AutomobileDao ad;

	public EmployeeService() {
	ed=new EmployeeDao();
	ad=new AutomobileDao();
	}
	
	
	public void createNewAutomobile() {
		Scanner scna = new Scanner(System.in);
	    
		System.out.println("\nWhat model is the vehicle?\n");
		String model = scna.nextLine();
		
		System.out.println("\nWhat is the vehicle's asking price?\n");
		
		float price = (float)(Math.round(scna.nextDouble()*100)/100.0);
		scna.nextLine();
		
		System.out.println("\nEnter the vehicle identification number\n");
		int vin = scna.nextInt();
	    
		ed.insertCar(vin, model, price);
		
		System.out.println("\nVehicle #"+vin+" was added to system\n");
		
	}

	public void createNewEmployee() {
		@SuppressWarnings("resource")
		Scanner scne = new Scanner(System.in);

		System.out.println("\nWelcome new employee\nEnter the admin password below\n");
		String adminPass = scne.nextLine();

		// verifies if the new user has the admins aproval
		if (adminPass.equals("Huge")) {
			System.out.println("\nEnter your username below\n");
			String usernameE = scne.nextLine();

			System.out.println("\nEnter your new password below\n");
			String passwordE = scne.nextLine();

			ed.insert(usernameE, passwordE);

			System.out.println("\nYour new account has been created " + usernameE + "\n");
		}

	}

	public void removeFromLot() {

		Scanner sr = new Scanner(System.in);
		System.out.println("Enter the VIN of the car you wish to remove from the lot:");
		Integer targetVin = sr.nextInt();

		ed.delete(targetVin);

		System.out.println("The vehicle with the vin# " + targetVin + " was removed");

	}

	

	public void showAllPayments() {
		List<Automobile> list = ad.findOwned();
		
		Iterator<Automobile> iterator = list.iterator();// Get an iterator for the list

	    while (iterator.hasNext())      // Check if there is another element
	    {
	        // Get the current element and move to the next one
	       Automobile i = iterator.next();
	       float price = i.getPrice();
	       int vin = i.getVin();
	       
	       float monthlyPayment =(float)(price/72);
	       float monthlyPayment2 = (float) (Math.round(monthlyPayment*100)/100.0);

	        System.out.println("The monthly payment for Vehicle#"+vin+" is $"+monthlyPayment2);
	    }
		
	}
	

	public boolean employeeVerifier() {

		Scanner s2 = new Scanner(System.in);

		System.out.println("\nEnter your username below:\n");
		String userID = s2.nextLine();

		System.out.println("\nEnter your password below:\n");

		String passwordAttempt = s2.nextLine();
		String passWordOnFile = ed.findById(userID);

		if (passwordAttempt.equals(passWordOnFile) == true) {
			return true;
		}

		return false;
	}

}
