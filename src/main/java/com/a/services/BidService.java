package com.a.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;
import com.a.dao.AutomobileDao;
import com.a.dao.BidDao;
import com.a.models.Automobile;
import com.a.models.Bid;

public class BidService {

	private BidDao bd;
	private AutomobileDao ad;

	public BidService() {
		bd = new BidDao();
		ad = new AutomobileDao();
	}
	
	public List<Bid> showAllBids() {
		 List<Bid> list= bd.findAll();
		 return list; 
	}

	public void placeBid(String u) {

		String customerId = u;
		Scanner spb = new Scanner(System.in);

		System.out.println("Please enter the VIN of the car you would like to place a bid on");
		int vin = spb.nextInt();
		spb.nextLine();

		System.out.println("Enter your bid ");
		float bid = spb.nextFloat();
		// spb.nextLine();
		// float bidf = (float) (Math.round(bid * 100) / 100.0);

		bd.insert(vin, customerId, bid);

	}

	public void acceptBid() {

		Scanner sab = new Scanner(System.in);

		System.out.println("Which bid would you like to accept?");
		System.out.println("Type 0 to quit");
		Integer target = sab.nextInt();
		if (target != 0) {

			int vin = bd.getVin(target);
			String username = bd.getUsername(target);
			float salesPrice = bd.getBidAmount(target);
			LocalDate dateOfSale = LocalDate.now();
			LocalDate endOfLoan = dateOfSale.plusMonths(72);
//			System.out.println(vin);
//			System.out.println(username);
//			System.out.println(salesPrice);
//			System.out.println(dateOfSale);
//			System.out.println(endOfLoan);

			ad.updateAutomobileAfterBid(vin, username, salesPrice, dateOfSale, endOfLoan);
			
			bd.deleteByVin(vin);
			System.out.println("Bid number " + target + " was accepted");
		}
	}

	public void rejectBid() {
		Scanner srb = new Scanner(System.in);

		System.out.println("Which bid would you like to reject?");
		int target = srb.nextInt();

		bd.delete(target);
		System.out.println("Bid number " + target + " was rejected");

	}
}
