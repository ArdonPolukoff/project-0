package com.a.services;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

import com.a.dao.AutomobileDao;
import com.a.dao.BidDao;

public class AutomobileService {
	
	private AutomobileDao ad;
	private BidDao bd;
	
	public AutomobileService() {
		ad = new AutomobileDao();
		bd = new BidDao();
	}
	
	public void calculateRemaingPayments(String cID) {
		Scanner scrp = new Scanner(System.in);
		LocalDate currentDate =  LocalDate.now();
		System.out.println("please enter the vin");
		int vin = scrp.nextInt();
		
		Date date= ad.getEndLoan(vin,cID);
		float price = ad.getPrice(vin, cID);
		int count = bd.getPaymentsCount();
		
		//takes date from db, converts to strin, then converts strin to localdate
		String dateS = date.toString();
		LocalDate endOfLoan = LocalDate.parse(dateS);
		Period remainingNumberOfPayments= Period.between(endOfLoan,currentDate);
		
		//converts local date from years and months to just months
		int years = remainingNumberOfPayments.getYears();
		int months = remainingNumberOfPayments.getMonths();
		int result = ((years * 12) + months)*(-1);
		
		float monthlyPayment =(float)(price/72);
		float monthlyPayment2 = (float) (Math.round(monthlyPayment*100)/100.0);
		
		System.out.println("There are " + count + "oustanding loans");
		System.out.println("You have "+result+" payments of $"+monthlyPayment2+ " remaining");
	}
	
	

}
