package com.a.util;

public class PlainTextConfig {
	
	private static final String url = System.getenv("URL");
	private static final String username = System.getenv("USER");
	private static final String password = System.getenv("PASS");
	
	
	public static String getUsername() {
		return username;
	}
	public static String getPassword() {
		return password;
	}
	public static String getUrl() {
		return url;
	}
	
	


}
