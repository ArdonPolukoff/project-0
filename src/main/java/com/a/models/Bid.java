package com.a.models;

public class Bid {
	
	float bidAmount;
	int bidId;
	int vin;
	String username;
	
	
	
	public Bid(float bidAmount, int bidId, int vin, String username) {
		super();
		this.bidAmount = bidAmount;
		this.bidId = bidId;
		this.vin = vin;
		this.username = username;
	}
	@Override
	public String toString() {
		return "[bidAmount=" + bidAmount + ", bidId=" + bidId + ", vin=" + vin + ", username=" + username + "]\n";
	}
	public float getBidAmount() {
		return bidAmount;
	}
	public void setBidAmount(float bidAmount) {
		this.bidAmount = bidAmount;
	}
	public int getBidId() {
		return bidId;
	}
	public void setBidId(int bidId) {
		this.bidId = bidId;
	}
	public int getVin() {
		return vin;
	}
	public void setVin(int vin) {
		this.vin = vin;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(bidAmount);
		result = prime * result + bidId;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		result = prime * result + vin;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bid other = (Bid) obj;
		if (Float.floatToIntBits(bidAmount) != Float.floatToIntBits(other.bidAmount))
			return false;
		if (bidId != other.bidId)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		if (vin != other.vin)
			return false;
		return true;
	}
	
	
	

	
	
	

}
