package com.a.models;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;

public class Automobile {
	
	int vin;
	String model;
	float price;
	String username;
	Date dateOfPurchase;
	Date endOfLoan;
	boolean isOwned;
	
	public Automobile(int vin, String model, float price, String username, Date dateOfPurchase,
			Date endOfLoan, boolean isOwned) {
		super();
		this.vin = vin;
		this.model = model;
		this.price = price;
		this.username = username;
		this.dateOfPurchase = dateOfPurchase;
		this.endOfLoan = endOfLoan;
		this.isOwned = isOwned;
	}
	
	




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateOfPurchase == null) ? 0 : dateOfPurchase.hashCode());
		result = prime * result + ((endOfLoan == null) ? 0 : endOfLoan.hashCode());
		result = prime * result + (isOwned ? 1231 : 1237);
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + Float.floatToIntBits(price);
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		result = prime * result + vin;
		return result;
	}






	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Automobile other = (Automobile) obj;
		if (dateOfPurchase == null) {
			if (other.dateOfPurchase != null)
				return false;
		} else if (!dateOfPurchase.equals(other.dateOfPurchase))
			return false;
		if (endOfLoan == null) {
			if (other.endOfLoan != null)
				return false;
		} else if (!endOfLoan.equals(other.endOfLoan))
			return false;
		if (isOwned != other.isOwned)
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (Float.floatToIntBits(price) != Float.floatToIntBits(other.price))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		if (vin != other.vin)
			return false;
		return true;
	}






	@Override
	public String toString() {
		return "[vin=" + vin + ", model=" + model + ", price=" + price + ", username="
				+ username + ", dateOfPurchase=" + dateOfPurchase + ", endOfLoan=" + endOfLoan + ", isOwned=" + isOwned
				+ "]\n";
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getVin() {
		return vin;
	}

	public void setVin(int vin) {
		this.vin = vin;
	}

	public boolean isOwned() {
		return isOwned;
	}

	public void setOwned(boolean isOwned) {
		this.isOwned = isOwned;
	}



	public Date getDateOfPurchase() {
		return dateOfPurchase;
	}



	public void setDateOfPurchase(Date dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}



	public Date getEndOfLoan() {
		return endOfLoan;
	}



	public void setEndOfLoan(Date endOfLoan) {
		this.endOfLoan = endOfLoan;
	}
	
	

	
	
}
