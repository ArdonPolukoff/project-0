package com.a.dao;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import com.a.models.Automobile;


public interface AutomobileDaoContract<T>{
	
	void updateAutomobileAfterBid(int v, String u, float p, LocalDate d, LocalDate el);
	Date getEndLoan(int t, String cID);
	float getPrice(int t, String cID);
	List<Automobile> findOwned();
	
	
}
