package com.a.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.a.util.ConnectionUtil;

public class EmployeeDao {
	public String findById(String s) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select employeepassword from employee where username = ? ";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, s);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getString("employeepassword");
		
	}catch(SQLException e) {
		e.printStackTrace();
	}
	return null;
}
	
	public void insert(String u, String p) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "INSERT INTO employee (username, employeepassword) VALUES (?,?)";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, u);
			ps.setString(2, p);
			ps.executeUpdate();
	}catch(SQLException e) {
		e.printStackTrace();
	}
}
	public void delete(int i) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "DELETE FROM automobile WHERE vin = ?";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, i);
			ps.executeUpdate();
	}catch(SQLException e) {
		e.printStackTrace();
	}
}
	public void insertCar(int v, String m, float p) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "INSERT INTO automobile" + 
					"(vin, model, price, username, dateofpurchase, endofloan, isowned)" + 
					"VALUES(?, ?, ?, null, null, null, false)";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, v);
			ps.setString(2, m);
			ps.setFloat(3, p);
			ps.executeUpdate();
	}catch(SQLException e) {
		e.printStackTrace();
	}
	//return null;
}
	


}
