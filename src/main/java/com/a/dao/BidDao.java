package com.a.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.a.models.Automobile;
import com.a.models.Bid;
import com.a.util.ConnectionUtil;

public class BidDao implements BidDaoContract<Bid> {
	
	public List<Bid> findAll() {
		// TODO Auto-generated method stub
		try {
			Connection conn = ConnectionUtil.connect();
			String sql = "select * from bid";
			List<Bid> list = new ArrayList<>();
			PreparedStatement ps;

			ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Bid(rs.getFloat("bidamount"),rs.getInt("bidid"),rs.getInt("vin"),rs.getString("username")));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	

	public void insert(int v, String u, float b) {

		try {
			Connection conn = ConnectionUtil.connect();
			String sql = "insert into bid "
					+ "(vin, username, bidamount) "
					+ "VALUES(?, ?, ?)";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, v);
			ps.setString(2, u);
			ps.setFloat(3, b);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void delete(int i) {
		// TODO Auto-generated method stub

		try {
			Connection conn = ConnectionUtil.connect();
			String sql = "DELETE FROM bid WHERE bidid = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, i);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteByVin(int i) {
		// TODO Auto-generated method stub

		try {
			Connection conn = ConnectionUtil.connect();
			String sql = "DELETE FROM bid WHERE vin = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, i);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String getUsername(int t) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select username from bid where bidid = ? ";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, t);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getString("username");
		
	}catch(SQLException e) {
		e.printStackTrace();
	}
	return null;
}
	
	public int getVin(int t) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select vin from bid where bidid = ? ";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, t);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getInt("vin");
		
	}catch(SQLException e) {
		e.printStackTrace();
	}
	return -1;
}
	
	public float getBidAmount(int t) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select bidamount from bid where bidid = ? ";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, t);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getFloat("bidamount");
		
	}catch(SQLException e) {
		e.printStackTrace();
	}
	return -1f;
}
	public int getPaymentsCount() {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql ="{ ? = call paymentsCount()}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.registerOutParameter(1, Types.INTEGER);
			cs.execute();
			int result = cs.getInt(1);
			cs.close();
			conn.close();
			return result;
	}catch(SQLException e) {
		e.printStackTrace();
	}
		return -1;
}
	

}
