package com.a.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

import com.a.models.Automobile;
import com.a.util.ConnectionUtil;



public class AutomobileDao implements AutomobileDaoContract<Automobile>{
	
	
	
	public void updateAutomobileAfterBid(int v, String u, float p, LocalDate d, LocalDate el) {
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "UPDATE automobile SET username = ?, price = ?, dateofpurchase = ?, endofloan= ?, isowned = ?  WHERE vin= ?";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, u);
			ps.setFloat(2, p);
			Date date = Date.valueOf(d);
			ps.setDate(3,date);
			Date end = Date.valueOf(el);
			ps.setDate(4, end);
			ps.setBoolean(5, true);
			ps.setInt(6, v);
			
			ps.executeUpdate();
			conn.close();
			ps.close();
			
		
	}catch(SQLException e) {
		e.printStackTrace();
	}

}
	public Date getEndLoan(int t, String cID) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select endofloan from automobile where vin = ? and username = ?";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, t);
			ps.setString(2,cID);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getDate("endofloan");
		
	}catch(SQLException e) {
		e.printStackTrace();
	}
	return null;
}
	
	public float getPrice(int t, String cID) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select price from automobile where vin = ? and username = ?";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, t);
			ps.setString(2,cID);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getFloat("price");
		
	}catch(SQLException e) {
		e.printStackTrace();
	}
	return -1f;
}
	
	public List<Automobile> findOwned() {
		// TODO Auto-generated method stub
		try {
			Connection conn = ConnectionUtil.connect();
			String sql = "select * from automobile where isowned = true";
			List<Automobile> list = new ArrayList<>();
			PreparedStatement ps;

			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Automobile(rs.getInt("vin"), rs.getString("model"), 
			rs.getFloat("price"), rs.getString("username"), rs.getDate("dateofpurchase"), 
			rs.getDate("endofloan"), rs.getBoolean("isowned")));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	

}
