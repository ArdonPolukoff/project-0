package com.a.dao;

import java.util.List;



public interface CustomerDaoContract<T>{
	List<T> findAll();
	List<T> findByUsername(String s);
	String findById(String s);
	void insert(String u, String p);
}
