package com.a.dao;

import java.util.List;



public interface BidDaoContract<T> {
	
	List<T> findAll();
	void insert(int v, String u, float b);
	void delete(int i);
	void deleteByVin(int i);
	String getUsername(int t);
	int getVin(int t);
	float getBidAmount(int t);
	int getPaymentsCount();

}
