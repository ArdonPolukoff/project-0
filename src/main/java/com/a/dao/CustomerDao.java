package com.a.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.a.models.Automobile;
import com.a.util.ConnectionUtil;


public class CustomerDao  {

	public List<Automobile> findAll() {
		// TODO Auto-generated method stub
		try {
			Connection conn = ConnectionUtil.connect();
			String sql = "select * from automobile where isowned = false";
			List<Automobile> list = new ArrayList<>();
			PreparedStatement ps;

			ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Automobile(rs.getInt("vin"), rs.getString("model"), 
			rs.getFloat("price"), rs.getString("username"), rs.getDate("dateofpurchase"), 
			rs.getDate("endofloan"), rs.getBoolean("isowned")));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Automobile> findByUsername(String s) {
		// TODO Auto-generated method stub
		try {
			Connection conn = ConnectionUtil.connect();
			String sql = "select * from automobile where username = ?";
			List<Automobile> list = new ArrayList<>();
			PreparedStatement ps;

			ps = conn.prepareStatement(sql);
			ps.setString(1, s);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Automobile(rs.getInt("vin"), rs.getString("model"), 
			rs.getFloat("price"), rs.getString("username"), rs.getDate("dateofpurchase"), 
			rs.getDate("endofloan"), rs.getBoolean("isowned")));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public String findById(String s) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select customerpassword from customer where username = ? ";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, s);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getString("customerpassword");
		
	}catch(SQLException e) {
		e.printStackTrace();
	}
	return null;
}
	
	public void insert(String u, String p) {
		// TODO Auto-generated method stub
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "INSERT INTO customer (username, customerpassword) VALUES (?,?)";
		
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, u);
			ps.setString(2, p);
			//ResultSet rs = ps.executeQuery();
			//rs.next();
			//return rs.getString("u");
			ps.executeUpdate();
	}catch(SQLException e) {
		e.printStackTrace();
	}
	//return null;
}
	

}
