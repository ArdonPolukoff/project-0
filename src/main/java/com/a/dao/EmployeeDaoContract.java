package com.a.dao;

public interface EmployeeDaoContract<T> {
	String findById(String s);
	void insert(String u, String p);
	void delete(int i);
	void insertCar(int v, String m, float p);
}
